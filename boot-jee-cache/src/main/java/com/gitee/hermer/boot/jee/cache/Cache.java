package com.gitee.hermer.boot.jee.cache;

import java.util.List;


public interface Cache {

	
	public Object get(Object key) throws CacheException;
	
	
	public void put(Object key, Object value) throws CacheException;
	
	
	public void put(Object key, Object value, Integer expireInSec) throws CacheException;
	
	
	public void update(Object key, Object value) throws CacheException;
	
	
	public void update(Object key, Object value, Integer expireInSec) throws CacheException;

	@SuppressWarnings("rawtypes")
	public List keys() throws CacheException ;
	
	
	public void evict(Object key) throws CacheException;
	
	
	@SuppressWarnings("rawtypes")
	public void evict(List keys) throws CacheException;
	
	
	public void clear() throws CacheException;
	
	
	public void destroy() throws CacheException;
	
}
