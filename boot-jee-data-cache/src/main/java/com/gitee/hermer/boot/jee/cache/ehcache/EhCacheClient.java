package com.gitee.hermer.boot.jee.cache.ehcache;

import java.util.List;
import com.gitee.hermer.boot.jee.cache.Cache;
import com.gitee.hermer.boot.jee.cache.CacheListener;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;
/**
 * 
 * @ClassName: EhCacheClient
 * @Description: ehCache缓存实现
 * @author:  涂孟超
 * @date: 2017年10月24日 上午11:15:24
 */
public class EhCacheClient extends UtilsContext implements Cache,CacheEventListener{
	
	private net.sf.ehcache.Cache cache;
	private CacheListener listener;
	

	public EhCacheClient(net.sf.ehcache.Cache cache, CacheListener listener) {
		super();
		this.cache = cache;
		this.listener = listener;
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List keys() throws CacheException {
		return this.cache.getKeys();
	}

	
	@Override
	public Object get(Object key) throws CacheException {
		try {
			if ( key == null ) 
				return null;
			else {
                Element element = cache.get( key );
				if ( element != null )
					return element.getObjectValue();				
			}
			return null;
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}
	}

	
	@Override
	public void update(Object key, Object value) throws CacheException {
		put( key, value );
	}

	
	@Override
	public void put(Object key, Object value) throws CacheException {
		try {
			Element element = new Element( key, value );
			cache.put( element );
		}
		catch (IllegalArgumentException e) {
			throw new CacheException( e );
		}
		catch (IllegalStateException e) {
			throw new CacheException( e );
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}

	}

	
	@Override
	public void evict(Object key) throws CacheException {
		try {
			cache.remove( key );
		}
		catch (IllegalStateException e) {
			throw new CacheException( e );
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}
	}

	
	@Override
	@SuppressWarnings("rawtypes")
	public void evict(List keys) throws CacheException {
		cache.removeAll(keys);
	}

	
	@Override
	public void clear() throws CacheException {
		try {
			cache.removeAll();
		}
		catch (IllegalStateException e) {
			throw new CacheException( e );
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}
	}

	
	

	

	@Override
	public void notifyRemoveAll(Ehcache arg0) {}

	@Override
	public void put(Object key, Object value, Integer expireInSec) throws CacheException {
		try {
			Element element = new Element( key, value );
			element.setTimeToLive(expireInSec);
			cache.put(element );
		}
		catch (IllegalArgumentException e) {
			throw new CacheException( e );
		}
		catch (IllegalStateException e) {
			throw new CacheException( e );
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}
	}
	@Override
	public void destroy() throws CacheException {
		try {
			cache.getCacheManager().removeCache( cache.getName() );
		}
		catch (IllegalStateException e) {
			throw new CacheException( e );
		}
		catch (net.sf.ehcache.CacheException e) {
			throw new CacheException( e );
		}
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException { 
		throw new CloneNotSupportedException(); 
	}

	@Override
	public void update(Object key, Object value, Integer expireInSec) throws CacheException {
		put(key, value, expireInSec);
	}
	@Override
	public void dispose() {}

	@Override
	public void notifyElementEvicted(Ehcache arg0, Element arg1) {}

	@Override
	public void notifyElementExpired(Ehcache cache, Element elem) {
		if(listener != null){
			listener.notifyCacheExpired(cache.getName(), elem.getObjectKey());
		}
	}

	@Override
	public void notifyElementPut(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {}

	@Override
	public void notifyElementRemoved(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {}

	@Override
	public void notifyElementUpdated(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {}


}
