package com.gitee.hermer.boot.jee.generate.controller;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.utils.FileUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;
import com.gitee.hermer.boot.jee.generate.properties.GenerateProperties;
import com.gitee.hermer.boot.jee.generate.template.BjeTemplateUtils;

public class GenerateController {
	public static void generate(){
		if(StringUtils.isNotBlank(GenerateProperties.getCONTROLLE_PACKAGE_NAME())){
			File file = new File(GenerateProperties.getPROJECT_PATH()+"/main/java/"+File.separator+GenerateProperties.getCONTROLLE_PACKAGE_NAME().replaceAll("\\.", "\\\\"));
			file.mkdirs();
			String className = GenerateProperties.getCLASS_NAME()+"RestContoller.java";
			
			Map<String, String> params = CollectionUtils.newHashMap();
			
			params.put("package-name", GenerateProperties.getCONTROLLE_PACKAGE_NAME());
			params.put("entity-name", GenerateProperties.getCLASS_NAME());
			StringCache cache = new StringCache("import "+GenerateProperties.getENTITY_PACKAGE_NAME()).append(".").append(GenerateProperties.getCLASS_NAME()).append(";\n");
			cache.append("import ").append(GenerateProperties.getSERVICE_PACKAGE_NAME()).append(".").append(GenerateProperties.getCLASS_NAME()).append("ServiceImpl;\n");
			params.put("import-package", cache.toString());
			
			params.put("request-url", GenerateProperties.getCLASS_ALIAS());
			
			String text = BjeTemplateUtils.template("rest-contoller-template.bje", params);
			
			try {
				FileUtils.write(file.getPath()+File.separator+className, text, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
}
