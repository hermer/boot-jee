package com.gitee.hermer.boot.jee.generate.service;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import com.gitee.hermer.boot.jee.commons.collection.CollectionUtils;
import com.gitee.hermer.boot.jee.commons.collection.StringCache;
import com.gitee.hermer.boot.jee.commons.utils.FileUtils;
import com.gitee.hermer.boot.jee.commons.utils.StringUtils;
import com.gitee.hermer.boot.jee.generate.properties.GenerateProperties;
import com.gitee.hermer.boot.jee.generate.template.BjeTemplateUtils;

public class GenerateService {

	public static void generate(){
		if(StringUtils.isNotBlank(GenerateProperties.getSERVICE_PACKAGE_NAME()))
		{
			File file = new File(GenerateProperties.getPROJECT_PATH()+"/main/java/"+File.separator+GenerateProperties.getSERVICE_PACKAGE_NAME().replaceAll("\\.", "\\\\"));
			file.mkdirs();
			String serviceName = GenerateProperties.getCLASS_NAME()+"ServiceImpl.java";
			
			Map<String, String> params = CollectionUtils.newHashMap();
			
			params.put("package-name", GenerateProperties.getSERVICE_PACKAGE_NAME());
			params.put("entity-name", GenerateProperties.getCLASS_NAME());
			
			StringCache cache = new StringCache("import "+GenerateProperties.getENTITY_PACKAGE_NAME()).append(".").append(GenerateProperties.getCLASS_NAME()).append(";\n");
			cache.append("import ").append(GenerateProperties.getDAO_PACKAGE_NAME()).append(".").append("I").append(GenerateProperties.getCLASS_NAME()).append("Dao;\n");
			params.put("import-package", cache.toString());
			
			String text = BjeTemplateUtils.template("service-template.bje", params);
			try {
				FileUtils.write(file.getPath()+File.separator+serviceName, text, false);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}    
	}

}
