package com.gitee.hermer.boot.jee.commons.verify;

import java.util.Collection;
import java.util.Map;

import com.gitee.hermer.boot.jee.commons.exception.ErrorCode;
import com.gitee.hermer.boot.jee.commons.exception.PaiUException;
import com.gitee.hermer.boot.jee.commons.exception.PaiUException.IErrorCode;


public class Assert {

	
	public static void isTrue(boolean expression) throws PaiUException{
		isTrueCode(expression,ErrorCode.VERIFY_ERROR);
	}
	
	public static void isNull(Object object) throws PaiUException{
		isNullCode(object,ErrorCode.VERIFY_ERROR);
	}
	
	public static void hasText(String text) throws PaiUException{
		hasTextCode(text, ErrorCode.VERIFY_ERROR);
	}
	
	public static void notNull(Object text) throws PaiUException{
		notNullCode(text, ErrorCode.VERIFY_ERROR);
	}
	
	public static void isEmpty(Collection collection) throws PaiUException{
		isEmptyCode(collection, ErrorCode.VERIFY_ERROR);
	}
	
	public static void notEmpty(Collection collection) throws PaiUException{
		notEmptyCode(collection, ErrorCode.VERIFY_ERROR);
	}
	
	public static void notEmpty(Map map) throws PaiUException{
		notEmptyCode(map, ErrorCode.VERIFY_ERROR);
	}
	
	
	public static void isTrueCode(boolean expression,IErrorCode errorCode) throws PaiUException{
		isTrueCode(expression,errorCode,null);
	}

	public static void isTrueCode(boolean expression,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.isTrue(expression);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}

	public static void isNullCode(Object object,IErrorCode errorCode) throws PaiUException{
		isNullCode(object,errorCode,null);
	}

	public static void isNullCode(Object object,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.isNull(object);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	
	public static void hasTextCode(String object,IErrorCode errorCode) throws PaiUException{
		hasTextCode(object,errorCode,null);
	}

	public static void hasTextCode(String object,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.hasText(object);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	
	public static void isEmptyCode(Collection list,IErrorCode errorCode) throws PaiUException{
		isEmptyCode(list,errorCode,null);
	}
	public static void isEmptyCode(Collection list,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			notNullCode(list, errorCode,formatArgs);
			if(list.size() != 0)
				throw new Exception();
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	
	public static void notEmptyCode(Collection list,IErrorCode errorCode) throws PaiUException{
		notEmptyCode(list,errorCode,null);
	}

	public static void notEmptyCode(Collection list,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.notEmpty(list);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	public static void notEmptyCode(Map list,IErrorCode errorCode) throws PaiUException{
		notEmptyCode(list,errorCode,null);
	}

	public static void notEmptyCode(Map list,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.notEmpty(list);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	public static void notEmptyCode(Object[] list,IErrorCode errorCode) throws PaiUException{
		notEmptyCode(list,errorCode,null);
	}

	public static void notEmptyCode(Object[] list,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.notEmpty(list);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	public static void hasLengthCode(String text,IErrorCode errorCode) throws PaiUException{
		hasLengthCode(text,errorCode,null);
	}

	public static void hasLengthCode(String text,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.hasLength(text);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
	
	
	public static void notNullCode(Object object,IErrorCode errorCode) throws PaiUException{
		notNullCode(object,errorCode,null);
	}

	public static void notNullCode(Object object,IErrorCode errorCode,String... formatArgs) throws PaiUException{
		try{
			org.springframework.util.Assert.notNull(object);
		}catch (Exception e) {
			throw new PaiUException(errorCode,e,formatArgs);
		}
	}
}
