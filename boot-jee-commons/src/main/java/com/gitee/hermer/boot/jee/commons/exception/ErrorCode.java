package com.gitee.hermer.boot.jee.commons.exception;

import com.gitee.hermer.boot.jee.commons.exception.PaiUException.IErrorCode;

public enum ErrorCode implements IErrorCode{
	SYSTEM_ERROR("S000","系统异常：[%s]"),
	PAGE_NO_FIND_ERROR("S100","页面没找到."),
	DATA_ERROR("D000","数据异常：[%s]"),
	VERIFY_ERROR("V000","验证异常");  
	
	private String msg;
	private String code;
	
	ErrorCode(String code,String msg){
		this.msg = msg;
		this.code = code;
	}

	@Override
	public String getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return msg;
	}

}
