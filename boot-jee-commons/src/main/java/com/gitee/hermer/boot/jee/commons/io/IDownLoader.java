
package com.gitee.hermer.boot.jee.commons.io;

import java.io.File;


public interface IDownLoader {

	/**
	 * 下载
	 * @param url 下载地址
	 * @param path 下载路径,如果为Null,或者为“”，将默认当前工作路径
	 * @param timeout 超时时间 ，0的话为不限制时间，直到程序出现异常
	 * @return 下载后文件路径
	 * @throws Exception
	 */
	public File downLoad(String url, String path, int timeout) throws Exception;

}
