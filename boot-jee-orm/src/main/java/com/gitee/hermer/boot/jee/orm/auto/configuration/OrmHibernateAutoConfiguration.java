package com.gitee.hermer.boot.jee.orm.auto.configuration;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import com.gitee.hermer.boot.jee.commons.log.UtilsContext;



@Configuration     
@ConditionalOnProperty(prefix = "com.boot.jee.orm",value = "factory", havingValue = "hibernate", matchIfMissing = false) 
public class OrmHibernateAutoConfiguration extends UtilsContext {


	@Autowired 
	private DataSource dataSource;

}
