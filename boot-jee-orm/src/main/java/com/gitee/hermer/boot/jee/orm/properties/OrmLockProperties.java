package com.gitee.hermer.boot.jee.orm.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "com.boot.jee.orm.optimistic.lock")  
public class OrmLockProperties {
	
	private String versionColumn = "version";
	private String versionField = "version";
	
	@ConfigurationProperties(value="version-column")
	public String getVersionColumn() {
		return versionColumn;
	}
	public void setVersionColumn(String versionColumn) {
		this.versionColumn = versionColumn;
	}
	@ConfigurationProperties(value="version-field")
	public String getVersionField() {
		return versionField;
	}
	public void setVersionField(String versionField) {
		this.versionField = versionField;
	}
	
}
