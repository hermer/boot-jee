package com.gitee.hermer.boot.jee.api.domain;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

import com.gitee.hermer.boot.jee.commons.exception.PaiUException;
import com.gitee.hermer.boot.jee.commons.utils.DateUtils;

public class BaseInfo<T> implements Serializable{
	

private static final long serialVersionUID = -8419326403652977068L;
	

	@ApiModelProperty(value="应用协议的版本",required=true)
	private String protocolVersion;	//应用协议的版本

	@ApiModelProperty(value="消息计数器时间戳",required=true)
	private String requestId;		//消息计数器时间戳

	@ApiModelProperty(value="响应的数据体")
	private T data;            		//请求的数据

	@ApiModelProperty(value="响应的数据体集合")
	private List<T> list;            		//请求的数据
	
	private String errorCode;   	//消息码

	@ApiModelProperty(value="错误消息描述")
	private String error;     		//消息

	@ApiModelProperty(value="是否成功",required=true)
	private Boolean success;         //是否成功

	
	
	
	public BaseInfo(String protocolVersion,List<T> list) {
		this(protocolVersion);
		this.list = list;
		this.success = Boolean.TRUE;
	}
	
	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}


	public BaseInfo(String protocolVersion) {
		this.requestId = DateUtils.getCurrentTimeStamp();
		this.protocolVersion = protocolVersion;
	}
	

	public BaseInfo(String protocolVersion,Boolean success) {
		this(protocolVersion);
		this.success = success;
	}
	public BaseInfo(String protocolVersion,T data) {
		this(protocolVersion);
		this.data = data;
		this.success = Boolean.TRUE;
	}
	
	public BaseInfo(String protocolVersion,PaiUException e)
	{
		this(protocolVersion);
		this.success = Boolean.FALSE;
		setErrorCode(e.getErrorCode(),e.getMessage());
	}

	public String getProtocolVersion() {
		return protocolVersion ;
	}
	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode,String errorMsg) {
		this.errorCode = errorCode;
		this.error = errorMsg;
	}
	
	
	public String getError() {
		return error;
	}
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append("BaseInfo >> : ");
		sb.append("[\n");
		if(protocolVersion != null && !"".equals(protocolVersion))
		{			
			sb.append("protocolVersion : " + getProtocolVersion() + ",\n");
		}
		if(requestId != null && !"".equals(requestId))
		{			
			sb.append("requestId : " + requestId + ",\n");
		}
		if(data != null && !"".equals(data))
		{			
			sb.append("data : " + data.toString() + ",\n");
		}		
		if(errorCode != null && !"".equals(errorCode))
		{			
			sb.append("errorCode : " + errorCode + ",\n");
		}
		if(error != null && !"".equals(error))
		{			
			sb.append("error : " + error + ",\n");
		}
		if(success != null && !"".equals(success))
		{			
			sb.append("success : " + success + ",\n");
		}
		sb.append("]");
		return sb.toString();
	}	
	
	
}

